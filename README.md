## E-VOA Admin Dashboard

This is an admin dashboard created by using:

1. [Vue cli4](https://cli.vuejs.org/)
2. [Vuex](https://vuex.vuejs.org/)
3. [Element UI](https://element.eleme.io/#/en-US)

### Development Guideline

#### Scripts

```bash

# install dependencies
npm install

# start the project
npm run serve

```

#### Project Directories

```text
|-evoa-admin
          |
          |-assets              # fonts and images
          |
          |-components          # components
          |
          |-interfaces          # data structure from api
          |
          |-layout              # overrall layout
          |
          |-store               # component data communication
          |
          |-style               # global style
          |
          |-utils               # common functions that can be applied to project
          |

```

#### Module / Page Folder Structure

> under /modules folder, you may naming your module
> as user-list, and just like structure below:

```text
├─evisa
    │─  index.vue    # parenet routing component
    |─  evisa.vue    #component to display evisa information
    │
    |
```

#### Vuex Folder Structure

> under /store folder, you may create two folders
> as actions and reducers, the folder structure are just like below:

```text
├─store
    │
    ├─modules
    |    app.ts            # data store open close side bar
    |    Evisa.ts          # data store evisa
    |    Immigration.ts    # data store Immigration
    |    ImmigrationDetail.ts # data store Immigration
    |    UsersManagement.ts #data store users information
    |    VisaDetail.ts      #data store visa detail
    
```
