interface IResponse<D> {
  status: number;
  data: D;
}

export { IResponse };
