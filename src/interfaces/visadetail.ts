import { IAccommodation, IApplicant, IDocument, ITravelDetail } from './evisa';
import { IResponse } from './response';

interface IVisaDetail {
  accommodation: IAccommodation;
  applicants: Array<IApplicant>;
  applicationStatus: string;
  approvals: Array<IApproval>;
  countryOrRegion: string;
  document: Array<IDocument>;
  duration: number;
  entryDate: string;
  exitDate: string;
  id: string;
  portOfArrival: string;
  purposeOfVisit: string;
  travelDetails: ITravelDetail;
  userId: number;
  visaType: string;
}

interface IApproval {
  approvalDateTime: number;
  approvalStatus: string;
  documentUrl: string;
  referenceNumber: string;
  travelDocumentNumber: string;
}

type IVisaDetailReponse = IResponse<IVisaDetail>;

export { IVisaDetail, IVisaDetailReponse, IApproval };
