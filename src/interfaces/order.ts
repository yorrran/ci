import { IApplicant } from './evisa';
import { IResponse } from './response';
interface IOrder {
  order_id: string;
  trade_no: string;
  total_amount: ICurrency;
  tenant_id: string;
  user_id: string;
  accommodation_info: IAccommodation;
  applicants: Array<IApplicant>;
  sku_id: number;
}

interface ICurrency {
  currency_code: string;
  units: string;
  nanos: number;
}

enum EOrderStatus {
  STATUS_INIT = 'STATUS_INIT',
  STATUS_PAYING = 'STATUS_PAYING',
  STATUS_PAY_OK = 'STATUS_PAY_OK',
  STATUS_PAY_FAILED = 'STATUS_PAY_FAILED',
  STATUS_EXPIRED = 'STATUS_EXPIRED',
  STATUS_VERIFICATION_OK = 'STATUS_VERIFICATION_OK',
  STATUS_VERIFICATION_FAILED = 'STATUS_VERIFICATION_FAILED',
  STATUS_CANCELED = 'STATUS_CANCELED',
  STATUS_SUBMITTED = 'STATUS_SUBMITTED',
  STATUS_APPROVED = 'STATUS_APPROVED',
  STATUS_FAILED = 'STATUS_FAILED',
}

interface IOrderData {
  order_data: IOrder;
  payment_status: EOrderStatus;
  status: EOrderStatus;
}

interface IOrderInfo {
  order_infos: Array<IOrderData>;
  page: { offset: number; limit: number; total: number };
}

interface IAccommodation {
  accommodation_image_url: string;
  city: string;
  district: string;
  hotel_address: string;
  hotel_name: string;
  refer_person_address: string;
  refer_person_name: string;
  subdistrict: string;
  zipcode: string;
}

type IOrderReponse = IResponse<IOrderInfo>;

export {
  IOrder,
  IOrderInfo,
  EOrderStatus,
  IOrderData,
  IOrderReponse,
  IAccommodation,
};
