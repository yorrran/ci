import { IResponse } from './response';
import { IAccommodation, IApplicant, IDocument, ITravelDetail } from './evisa';

interface IImigration {
  accommodation: IAccommodation;
  applicants: IApplicant;
  applicationStatus: string;
  approvals: string;
  countryOrRegion: string;
  documents: Array<IDocument>;
  duration: Number;
  entryDate: String;
  exitDate: String;
  id: String;
  portOfArrival: string;
  purposeOfVisit: string;
  travelDetails: ITravelDetail;
  userId: string;
  visaType: string;
}

type IImigrationReponse = IResponse<Array<IImigration>>;

export { IImigration, IImigrationReponse };
