import { IAccommodation, IApplicant, IDocument, ITravelDetail } from './evisa';
import { IResponse } from './response';
import { IApproval } from './visadetail';

interface IImmigrationDetail {
  accommodation: IAccommodation;
  applicants: Array<IApplicant>;
  applicationStatus: string;
  approvals: Array<IApproval>;
  countryOrRegion: string;
  document: Array<IDocument>;
  duration: number;
  entryDate: string;
  exitDate: string;
  id: string;
  portOfArrival: string;
  purposeOfVisit: string;
  travelDetails: ITravelDetail;
  userId: number;
  visaType: string;
}

type IImmigrationDetailResponse = IResponse<IImmigrationDetail>;

export { IImmigrationDetail, IImmigrationDetailResponse };
