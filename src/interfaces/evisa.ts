import { IResponse } from './response';

interface IEVisa {
  accommodation: IAccommodation;
  applicants: IApplicant;
  applicationStatus: string;
  approvals: string;
  countryOrRegion: string;
  documents: Array<IDocument>;
  duration: Number;
  entryDate: String;
  exitDate: String;
  id: String;
  portOfArrival: string;
  purposeOfVisit: string;
  travelDetails: ITravelDetail;
  userId: string;
  visaType: string;
}

interface IAccommodation {
  accommodationName: string;
  address: string;
  city: string;
  district: string;
  referencePersonAddress: string;
  dress: string;
  referencePersonName: string;
  subDistrict: string;
  zipCode: string;
}

interface IApplicant {
  annualIncome: string;
  contactDetails: IContactDetail;
  id: string;
  occupation: string;
  passportInfo: IPassportInfo;
  purposeOfVisit: string;
  residenceAddress: string;
  userId: string;
}

interface IContactDetail {
  contactNumber: string;
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  phoneCountry: string;
  title: string;
}

interface IPassportInfo {
  countryOfBirth: string;
  dateOfBirth: string;
  firstName: string;
  gender: string;
  imageUrls: string;
  lastName: string;
  middleName: string;
  nationality: string;
  scannedImages: Array<IScannedImage>;
  travelDocumentExpiryDate: string;
  travelDocumentIssueCountry: string;
  travelDocumentIssueDate: string;
  travelDocumentIssuePlace: string;
  travelDocumentNumber: string;
  travelDocumentType: string;
}

interface IScannedImage {
  base64Content: string;
  contentType: string;
  documentType: string;
  fileName: string;
  id: string;
  referenceId: string;
  title: string;
  url: string;
}

interface IDocument {
  base64Content: string;
  contentType: string;
  fileName: string;
  id: string;
  referenceId: string;
  title: string;
  url: string;
}

interface ITravelDetail {
  arrivalAirport: string;
  arrivalCity: string;
  arrivalDate: string;
  arrivalTime: string;
  departAirport: string;
  departCountry: string;
  departDate: string;
  departFlightFNo: string;
  departFlightInfo: IDepartFlightInfo;
  returnDepartAirport: string;
  returnDepartDate: string;
  returnDepartTime: string;
  returnFlightInfo: IReturnFlightInfo;
  returnFlightNumber: string;
}

interface IDepartFlightInfo {
  arrivalAirport: IArrivalAirport;
  arrivalTerminal: string;
  arrivalTime: string;
  departureAirport: IDepartureAirport;
  departureTerminal: string;
  departureTime: string;
  flightNumber: string;
  referenceCode: string;
}

interface IReturnFlightInfo {
  arrivalAirport: IArrivalAirport;
  arrivalTerminal: string;
  arrivalTime: string;
  departureAirport: IDepartureAirport;
  departureTerminal: string;
  departureTime: string;
  flightNumber: string;
  referenceCode: string;
}

interface IArrivalAirport {
  city: string;
  cityCode: string;
  countryCode: string;
  countryName: string;
  fs: string;
  iata: string;
  icao: string;
  name: string;
  regionName: string;
  stateCode: string;
  timeZoneRegionName: string;
  utcOffsetHours: string;
}

interface IDepartureAirport {
  city: string;
  cityCode: string;
  countryCode: string;
  countryName: string;
  fs: string;
  iata: string;
  icao: string;
  name: string;
  regionName: string;
  stateCode: string;
  timeZoneRegionName: string;
  utcOffsetHours: string;
}

type IEvisaReponse = IResponse<Array<IEVisa>>;

export {
  IEVisa,
  IEvisaReponse,
  IAccommodation,
  IApplicant,
  IDocument,
  ITravelDetail,
  IReturnFlightInfo,
  IDepartFlightInfo,
  IPassportInfo,
};
