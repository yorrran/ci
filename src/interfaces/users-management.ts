import { IResponse } from './response';

interface IUser {
  createdTimestamp: number;
  emailVerified: boolean;
  enabled: boolean;
  id: string;
  username: string;
}

interface ICreateUser {
  username: string | null;
  email: string | null;
  firstName: string;
  lastName: string;
  enabled: boolean;
  emailVerified: boolean;
  requiredActions: Array<string>;
}

interface IEditUser {
  createdTimestamp: number;
  username: string | null;
  email: string | null;
  firstName: string;
  lastName: string;
  enabled: boolean;
  emailVerified: boolean;
  requiredActions: Array<string>;
}

interface IUpdatePassword {
  password: string;
  confirmPassword: string;
  temporary: boolean;
}

enum EUserActions {
  UPDATE_PASSWORD = 'UPDATE_PASSWORD',
  CONFIGURE_TOTP = 'CONFIGURE_TOTP',
  UPDATE_PROFILE = 'UPDATE_PROFILE',
  VERIFY_EMAIL = 'VERIFY_EMAIL',
  update_user_locale = 'update_user_locale',
}

type IRoleMapping = IEditUser & { aceess: any };
type IUsersResponse = IResponse<Array<IUser>>;
type IEditUserResponse = IResponse<IEditUser>;
type IRoleMappingResponse = IResponse<IRoleMapping>;

export {
  IUser,
  IUsersResponse,
  EUserActions,
  ICreateUser,
  IEditUser,
  IEditUserResponse,
  IUpdatePassword,
  IRoleMappingResponse,
};
