import Vue from 'vue';
import Router from 'vue-router';

// const Login = () =>
//   import(/* webpackChunkName: "login" */ '@/views/login/index.vue');
const Notfound = () =>
  import(/* webpackChunkName: "404" */ '@/components/404.vue');
const Layout = () =>
  import(/* webpackChunkName: "Layout" */ '@/layout/index.vue');
const EVisaEntry = () =>
  import(/* webpackChunkName: "EVisaEntry" */ '@/components/evisa/index.vue');
const EVisa = () =>
  import(/* webpackChunkName: "EVisa" */ '@/components/evisa/evisa.vue');
const VisaDetail = () =>
  import(
    /* webpackChunkName: "VisaDetail" */ '@/components/visa-detail/index.vue'
  );
const ImmigrationEntry = () =>
  import(
    /* webpackChunkName: "ImmigrationEntry" */ '@/components/Immigration/index.vue'
  );
const Immigration = () =>
  import(
    /* webpackChunkName: "Immigration" */ '@/components/Immigration/immigration.vue'
  );
const ImmigrationDetail = () =>
  import(
    /* webpackChunkName: "ImmigrationDetail" */ '@/components/immigration-detail/index.vue'
  );
const UsersEntry = () =>
  import(/* webpackChunkName: "UsersEntry" */ '@/components/users/index.vue');
const CreateUser = () =>
  import(
    /* webpackChunkName: "CreateUser" */ '@/components/users/create/create-user.vue'
  );
const Users = () =>
  import(/* webpackChunkName: "Users" */ '@/components/users/users.vue');
const EditUser = () =>
  import(
    /* webpackChunkName: "EditUser" */ '@/components/users/edit/index.vue'
  );
const Orders = () =>
  import(/* webpackChunkName: "EditUser" */ '@/components/Orders/index.vue');
// Vue.use(Router);

const router = new Router({
  mode: 'history', // Enable this if you need.
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/404',
      component: Notfound,
      meta: { hidden: true },
    },
    {
      name: 'Home',
      path: '/',
      component: Layout,
      redirect: '/orders',
      children: [
        {
          path: '/evisa',
          component: EVisaEntry,
          meta: {
            title: 'E-Visa',
            icon: 'el-icon-data-analysis',
          },
          children: [
            {
              path: '',
              component: EVisa,
            },
            {
              path: 'visa-detail/:id',
              component: VisaDetail,
              meta: {
                title: 'Detail',
              },
            },
          ],
        },
        {
          path: '/immigration',
          component: ImmigrationEntry,
          meta: {
            title: 'Immigration',
            icon: 'el-icon-data-analysis',
            type: 'IMMIGRATION',
          },
          children: [
            {
              path: '',
              component: Immigration,
            },
            {
              path: 'immigration-detail/:id',
              component: ImmigrationDetail,
              meta: {
                title: 'Detail',
              },
            },
          ],
        },
        {
          name: 'Users',
          path: '/users',
          component: UsersEntry,
          meta: {
            title: 'Users',
            icon: 'el-icon-user',
          },
          children: [
            {
              path: '',
              component: Users,
            },
            {
              path: 'create',
              component: CreateUser,
              meta: {
                title: 'Create',
              },
            },
            {
              path: 'edit/:id',
              component: EditUser,
              meta: {
                title: 'Edit',
              },
            },
          ],
        },
        {
          name: 'Orders',
          path: '/orders',
          component: Orders,
          meta: {
            title: 'Orders',
            icon: 'el-icon-shopping-cart-1',
          },
        },
      ],
    },
    {
      path: '*',
      redirect: '/404',
      meta: { hidden: true },
    },
  ],
});

export default router;
