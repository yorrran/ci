import Vue from 'vue';

import ElementUI from 'element-ui';

import '@/styles/element-variables.scss';
import '@/styles/index.scss';

import App from '@/App.vue';
import store from '@/store';
import Keycloak from 'keycloak-js';
import { setCloakToken, getToken, removeToken } from './utils/cookies';
// const keycloakJson = require('./keycloak/keyloak.json');
import Router from 'vue-router';
import router from './router';
import filters from './utils/filters';
import Component from 'vue-class-component';

export const keycloak = Keycloak('/keycloak.json');

Vue.use(ElementUI);
Vue.use(Router);
Vue.use(filters);
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteUpdate',
  'beforeRouteLeave',
]);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  // i18n,
  render: h => h(App),
}).$mount('#app');
