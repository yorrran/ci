import {
  IDepartFlightInfo,
  IReturnFlightInfo,
} from '../../../interfaces/evisa';
import { dateFormat } from '../../../utils/filters';

export const mapFlight = (
  currFlight: IDepartFlightInfo | IReturnFlightInfo
) => [
  {
    title: `Departure Date:`,
    value: currFlight
      ? dateFormat(currFlight.departureTime, 'YYYY MMM DD')
      : null,
  },
  {
    title: 'Departure Time:',
    value: currFlight ? dateFormat(currFlight.departureTime, 'HH:mm') : null,
  },
  {
    title: 'Departure City:',
    value:
      currFlight && currFlight.departureAirport
        ? currFlight.departureAirport.city
        : '',
  },
  {
    title: 'Departure Airport:',
    value:
      currFlight && currFlight.departureAirport
        ? currFlight.departureAirport.fs
        : '',
  },
  {
    title: 'Arrival Date:',
    value: dateFormat(currFlight.arrivalTime, 'YYYY MMM DD'),
  },
  {
    title: 'Arrival Time:',
    value: dateFormat(currFlight.arrivalTime, 'HH:mm'),
  },
  {
    title: 'Arrival City:',
    value:
      currFlight && currFlight.arrivalAirport
        ? currFlight.arrivalAirport.city
        : '',
  },
];
