import { IAccommodation } from '../../../interfaces/evisa';

export const mapAccomodation = (accomodation: IAccommodation) => [
  {
    title: `Accomodation Name:`,
    value:
      accomodation && accomodation.accommodationName
        ? accomodation.accommodationName
        : '',
  },
  {
    title: 'Address:',
    value: accomodation && accomodation.address ? accomodation.address : '',
  },
  {
    title: 'ZipCode:',
    value: accomodation && accomodation.zipCode ? accomodation.zipCode : '',
  },
  {
    title: 'Reference Person Name:',
    value:
      accomodation && accomodation.referencePersonName
        ? accomodation.referencePersonName
        : '',
  },
  {
    title: 'Referenece Person Address:',
    value:
      accomodation && accomodation.referencePersonName
        ? accomodation.referencePersonAddress
        : '',
  },
];
