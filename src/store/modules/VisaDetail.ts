import {
  VuexModule,
  Module,
  Action,
  Mutation,
  getModule,
} from 'vuex-module-decorators';
import store from '@/store';
import ajax from '../../utils/ajax';
import {
  IReturnFlightInfo,
  IDepartFlightInfo,
  IAccommodation,
  IApplicant,
} from '../../interfaces/evisa';
import { IVisaDetail } from '../../interfaces/visadetail';
import { IImigrationReponse } from '../../interfaces/immigration';

@Module({ dynamic: true, store, name: 'evoa/visa-detail' })
class VisaDetail extends VuexModule {
  departureFlightInfo = {} as IDepartFlightInfo;
  returnFlightInfo = {} as IReturnFlightInfo;
  arrivalTime: string = '';
  accommodation = {} as IAccommodation;
  applicants: Array<IApplicant> = [];

  @Action({ rawError: true })
  async getVisaDetail(id: string) {
    try {
      const res = await ajax.request<IVisaDetail>({
        method: 'GET',
        url: `/v1.0/evisa/applications/${id}`,
      });
      if (res.data.travelDetails) {
        this.updateReturnFlightinfo(res.data.travelDetails.returnFlightInfo);
        this.updateDepatureFlightinfo(res.data.travelDetails.departFlightInfo);
        this.updateAccomodation(res.data.accommodation);
        this.updateApplicants(res.data.applicants);
      }
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async apporveImmigrations(data: {
    id: string;
    actionType: string;
    comments: string;
  }) {
    try {
      const res = await ajax.request<{}, IImigrationReponse>({
        method: 'POST',
        url: `/v1.0/evisa/applications/${data.id}/approvals`,
        data: { actionType: 'APPROVE', comments: data.comments },
      });
      return res;
    } catch (e) {
      throw e;
    }
  }

  @Mutation
  private updateReturnFlightinfo(payload: IReturnFlightInfo) {
    this.returnFlightInfo = payload;
  }

  @Mutation
  private updateDepatureFlightinfo(payload: IDepartFlightInfo) {
    this.departureFlightInfo = payload;
    this.arrivalTime = payload.arrivalTime;
  }

  @Mutation
  private updateAccomodation(payload: IAccommodation) {
    this.accommodation = payload;
  }
  @Mutation
  private updateApplicants(payload: Array<IApplicant>) {
    this.applicants = [...payload];
  }
}

export const VisaDetailModule = getModule(VisaDetail);
