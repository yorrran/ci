import {
  VuexModule,
  Module,
  Action,
  Mutation,
  getModule,
} from 'vuex-module-decorators';
import store from '@/store';
import ajax from '../../utils/ajax';
import {
  IReturnFlightInfo,
  IDepartFlightInfo,
  IAccommodation,
  IApplicant,
} from '../../interfaces/evisa';
import { IImmigrationDetailResponse } from '../../interfaces/immigration-detail';

@Module({ dynamic: true, store, name: 'evoa/immigration-detail' })
class ImmigrationDetail extends VuexModule {
  departureFlightInfo = {} as IDepartFlightInfo;
  returnFlightInfo = {} as IReturnFlightInfo;
  accommodation = {} as IAccommodation;
  applicants: Array<IApplicant> = [];

  @Action({ rawError: true })
  async getImmigrationDetail(id: string) {
    try {
      const res = await ajax.request<{}, IImmigrationDetailResponse>({
        method: 'GET',
        url: `/v1.0/evisa/applications/${id}`,
      });
      this.updateReturnFlightinfo(res.data.travelDetails.returnFlightInfo);
      this.updateDepatureFlightinfo(res.data.travelDetails.departFlightInfo);
      this.updateAccomodation(res.data.accommodation);
      this.updateApplicants(res.data.applicants);
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async apporveImmigrations(data: { id: string; actionType: string }) {
    try {
      await ajax.request<{}>({
        method: 'PATCH',
        url: `/api/evisa/applicationRequests/${data.id}:updateStatus`,
        params: { approvalStatus: data.actionType },
        data: { method: 'PATCH' },
        headers: { 'Content-Type': 'application/json' },
      });
    } catch (e) {
      throw e;
    }
  }

  @Mutation
  private updateReturnFlightinfo(payload: IReturnFlightInfo) {
    this.returnFlightInfo = payload;
  }

  @Mutation
  private updateDepatureFlightinfo(payload: IDepartFlightInfo) {
    this.departureFlightInfo = payload;
  }

  @Mutation
  private updateAccomodation(payload: IAccommodation) {
    this.accommodation = payload;
  }

  @Mutation
  private updateApplicants(payload: Array<IApplicant>) {
    this.applicants = [...payload];
  }
}

export const ImmigrationDetailModule = getModule(ImmigrationDetail);
