import {
  VuexModule,
  Module,
  Action,
  Mutation,
  getModule,
} from 'vuex-module-decorators';
import store from '@/store';
import ajax from '../../utils/ajax';
import { IOrderReponse } from '../../interfaces/order';

@Module({ dynamic: true, store, name: 'evoa/orders' })
class Orders extends VuexModule {
  orderList: Array<IOrderReponse> = [];

  @Action({ rawError: true })
  async getOrders(data: { offset: number; search: string | null }) {
    try {
      const res = await ajax.request<{}, IOrderReponse>({
        method: 'GET',
        url: '/v1/tenants/1/order_status',
        params: {
          'page.limit': 10,
          'page.offset': data.offset,
          'page.total': 1,
          orderStatus: data.search,
        },
      });

      return res.data;
    } catch (e) {
      console.log('data');
    }
  }

  @Action({ rawError: true })
  async updateOrderStatus(data: { id: string; status: string }) {
    console.log('order');
    try {
      const res = await ajax.request<{}, IOrderReponse>({
        method: 'PUT',
        url: `/v1/tenants/1/orders/${data.id}`,
        data: {
          order_id: data.id,
          order_status: data.status,
          tenant_id: 1,
        },
      });

      return res.data;
    } catch (e) {
      console.log('data');
    }
  }

  @Action({ rawError: true })
  async deleteOrder(id: string) {
    try {
      const res = await ajax.request<{}>({
        method: 'DELETE',
        url: `/v1/tenants/1/orders/${id}`,
      });
    } catch (e) {
      console.log('data');
    }
  }
}

export const OrdersModule = getModule(Orders);
