import { VuexModule, Module, Action, getModule } from 'vuex-module-decorators';
import store from '@/store';
import ajax from '../../utils/ajax';
import { IImigrationReponse, IImigration } from '../../interfaces/immigration';

@Module({ dynamic: true, store, name: 'evoa/immigration' })
class Immigration extends VuexModule {
  @Action({ rawError: true })
  async getImmigrations() {
    try {
      const res = await ajax.request<{}, IImigrationReponse>({
        method: 'GET',
        url: `/v1.0/evisa/applications`,
        params: { applicationStatus: 'PendingWithImmigration' },
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  }
}

export const ImmigrationModule = getModule(Immigration);
