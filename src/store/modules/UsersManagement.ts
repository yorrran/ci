import {
  VuexModule,
  Module,
  Action,
  Mutation,
  getModule,
} from 'vuex-module-decorators';
import store from '@/store';
import ajax from '../../utils/ajax';
import {
  IUser,
  IUsersResponse,
  ICreateUser,
  IEditUserResponse,
  IEditUser,
  IUpdatePassword,
} from '../../interfaces/users-management';

@Module({ dynamic: true, store, name: 'evoa/user-management' })
class Users extends VuexModule {
  @Action({ rawError: true })
  async getUsers(data: { first: number; search: string | null }) {
    const query = {
      briefRepresentation: true,
      first: data.first,
      max: 10,
    };
    const params = data.search ? { ...query, search: data.search } : query;
    try {
      const res = await ajax.request<{}, IUsersResponse>({
        method: 'GET',
        url: `/api/auth/admin/realms/E-VISA/users`,
        params: params,
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async getTotalNumberUsers(search: string | null) {
    const userQuery = search && search !== '' ? { search } : {};
    try {
      const res = await ajax.request<{}, IUsersResponse>({
        method: 'GET',
        url: `/api/auth/admin/realms/E-VISA/users`,
        params: userQuery,
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async createUser(data: ICreateUser) {
    try {
      const res = await ajax.request<{}>({
        method: 'POST',
        url: `/api/auth/admin/realms/E-VISA/users`,
        data: { ...data },
      });

      return res.data;
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async getUserById(id: string) {
    try {
      const res = await ajax.request<{}, IEditUserResponse>({
        method: 'GET',
        url: `/api/auth/admin/realms/E-VISA/users/${id}`,
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async updateUserById(data: IEditUser & { id: string }) {
    const { id, ...rest } = data;
    try {
      const res = await ajax.request<{}>({
        method: 'PUT',
        url: `/api/auth/admin/realms/E-VISA/users/${data.id}`,
        data: rest,
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async updatePassword(data: IUpdatePassword & { id: string }) {
    const { id, ...rest } = data;
    try {
      const res = await ajax.request<{}>({
        method: 'PUT',
        url: `/api/auth/admin/realms/E-VISA/users/${data.id}/reset-password`,
        data: rest,
      });
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async getAvaiableRoles(id: string) {
    try {
      const res = await ajax.request<{}>({
        method: 'GET',
        url: `/api/auth/admin/realms/E-VISA/users/${id}/role-mappings/realm/available`,
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async getAssignedRoles(id: string) {
    try {
      const res = await ajax.request<{}>({
        method: 'GET',
        url: `/api/auth/admin/realms/E-VISA/users/${id}/role-mappings/realm`,
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async submitAssignedRoles(data: { roleList: Array<string>; id: string }) {
    try {
      const res = await ajax.request<{}>({
        method: 'POST',
        url: `/api/auth/admin/realms/E-VISA/users/${data.id}/role-mappings/realm`,
        data: data.roleList.map(role => ({ name: role })),
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  }

  @Action({ rawError: true })
  async deleteUser(id: string) {
    try {
      const res = await ajax.request<{}>({
        method: 'DELETE',
        url: `/api/auth/admin/realms/E-VISA/users/${id}`,
      });
      return res.data;
    } catch (e) {
      throw e;
    }
  }

  @Mutation
  private updateReturnFlightinfo() {
    // this.returnFlightInfo = payload;
  }
}

export const UsersModule = getModule(Users);
