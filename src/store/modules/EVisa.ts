import {
  VuexModule,
  Module,
  Action,
  Mutation,
  getModule,
} from 'vuex-module-decorators';
import store from '@/store';
import ajax from '../../utils/ajax';
import { IEvisaReponse, IEVisa } from '@/interfaces/evisa';

@Module({ dynamic: true, store, name: 'evoa/e-visa' })
class EVisa extends VuexModule {
  eVisaList: Array<IEVisa> = [];

  @Action({ rawError: true })
  async getVisa() {
    try {
      const res = await ajax.request<{}, IEvisaReponse>({
        method: 'GET',
        url: '/v1.0/evisa/applications',
        params: { applicationStatus: 'Submitted' },
      });

      return res.data;
    } catch (e) {
      throw e;
    }
  }
}

export const EVisaModule = getModule(EVisa);
