import axios from 'axios';
import { Message, MessageBox } from 'element-ui';
import qs from 'query-string';

const instance = axios.create({
  baseURL: '',
  timeout: 5000,
  paramsSerializer: params => {
    return qs.stringify(params);
  },
});

// Request interceptors
instance.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    throw error;
  }
);

// Response interceptors
instance.interceptors.response.use(
  response => {
    const res = response.data;
    if (response.status !== 200 && response.status !== 201) {
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000,
      });
      if (
        response.status === 50008 ||
        response.status === 50012 ||
        response.status === 50014
      ) {
        MessageBox.confirm(
          'You have been logged out, try to login again.',
          'Log out',
          {
            confirmButtonText: 'Relogin',
            cancelButtonText: 'Cancel',
            type: 'warning',
          }
        ).then(() => {
          // UserModule.ResetToken();
          // location.reload(); // To prevent bugs from vue-router
        });
      }
      if (response.status === 403) {
        console.log('status:', response.status);
        // window.location.reload();
      }
      if (response.status === 401) {
        console.log('error:', response.status);
        // window.location.reload();
      }
      return Promise.reject(new Error(res.message || 'Error'));
    } else {
      return response;
    }
    // return response;
  },
  error => {
    Message({
      message: 'it is error',
      type: 'error',
      duration: 5 * 1000,
    });
    console.log('error:', error.code);

    // window.location.reload();
    // window.location.reload();
    return Promise.reject(error);
  }
);

export default instance;
