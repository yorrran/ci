import Cookies from 'js-cookie';
import ajax from './ajax';

// App
const sidebarStatusKey = 'sidebar_status';
export const getSidebarStatus = () => Cookies.get(sidebarStatusKey);
export const setSidebarStatus = (sidebarStatus: string) =>
  Cookies.set(sidebarStatusKey, sidebarStatus);

// User
const tokenKey = 'admin_access_token';
export const getToken = () => Cookies.get(tokenKey);
export const setToken = (token: string) => Cookies.set(tokenKey, token);
export const removeToken = () => Cookies.remove(tokenKey);

export const setCloakToken = (keycloakToken: string | undefined) => {
  keycloakToken && setToken(keycloakToken);
  ajax.defaults.headers.common['Authorization'] = `Bearer ${keycloakToken}`;
};
