import moment from 'moment';
import { VueConstructor } from 'vue';

export function dateFormat(value: string, format: string) {
  if (!value) {
    return null;
  } else {
    switch (format) {
      case 'YYYY MMM DD HH:mm':
        return moment(value).format('YYYY MMM DD HH:mm');
      case 'YYYY MMM DD':
        return moment(value).format('YYYY MMM DD');
      case 'HH:mm':
        return moment(value).format('HH:mm');
    }
  }
}

export default {
  install(Vue: VueConstructor) {
    Vue.filter('dateFormat', dateFormat);
  },
};
