const path = require ('path');
const name = 'Evoa Admin';

// 接口服务器地址
let crossHost = 'http://10.198.21.168:7901';

module.exports = {
  publicPath: '/',
  // ouputDir: dist,

  devServer: {
    disableHostCheck: true,
    // allowedHosts: ['10.151.101.14:6061'],
    host: '0.0.0.0', // 如果是真机测试，就使用这个IP
    port: 3000,
    hotOnly: false, // 热更新（webpack已实现了，这里false即可）
    proxy: {
      // 配置跨域(本地开发)
      '/v1': {
        target: crossHost,
        changeOrigin: true,
        https: false,
        ws: true,
        pathRewrite: {'/api': ''},
      },
    },
  },
  pwa: {
    name: name,
  },
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        path.resolve (__dirname, 'src/styles/_variables.scss'),
        path.resolve (__dirname, 'src/styles/_mixins.scss'),
      ],
    },
  },
  chainWebpack (config) {
    // Provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    config.set ('name', name);
  },
};
